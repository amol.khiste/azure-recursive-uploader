# Azure Storage Recursive Uploader

A C# WinForms application to recursive upload files and folders to Microsoft Azure™ Blob storage.

## Build

- Open the solution file AzureStorageRecursiveUploader.sln in VS2013 Update 5, or higher.
- The solution contains two projects:
  - **UploadToStorage:** A C# Console application. Creates output *azureuploader.exe*. This can be used by itself by passing command line arguments or with **UploadUI** below.
  - **UploadUI:** A C# WinForms application. Creates output *azureuploaderui.exe*, which uses *azureuploader.exe* for uploading.
- Output of **UploadToStorage** is copied to **UploadUI**'s output directory in post-build step, so that *azureuploaderui.exe* can access *azureuploader.exe* relatively.

## Installation

Sorry, there is no installer yet. Please see build steps above to build and run the application. To share the application, copy the files in the **UploadUI** project's output folder.

## Usage

### From UI
- Run *azureuploaderui.exe* (from **UploadUI**'s output folder).
- Enter your Azure storage account details and container name to which files are to be uploaded.
- Select folder to upload recursively.
- Click 'Upload'.

    ![ui screenshot](azureuploaderui.png "UI screenshot")

### From command line
- Open command window and go to UploadToStorage folder in **UploadUI**'s output folder.
- Run *azureuploader.exe* with following arguments:

        -n, --account-name          Required. Azure Storage Account Name.
        -k, --account-key           Required. Azure Storage Account Key.
        -s, --protocol              (Default: https) Endpoint protocol to use (e.g. 'http', 'https'). Default is https.
        -c, --container-name        Required. Root blob container in which to upload.
        -f, --upload-folder-path    Required. Path of folder to upload.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

04/Feb/2016: First release.

## Credits

- **Joseph Fultz** for Auzre blob storage upload code shared [here](http://blogs.msdn.com/b/joseph_fultz/archive/2014/10/20/recursive-upload-from-disk-to-azure-storage.aspx).
- **Yves Goergen** for System menu helper class shared [here](https://github.com/dg9ngf/FieldLog/blob/master/LogSubmit/Unclassified/UI/SystemMenu.cs).

## License

Please see LICENSE file.
