﻿namespace Geometric.Azure.RecursiveUploader.UI
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.accountNameLabel = new System.Windows.Forms.Label();
            this.accountNameTextBox = new System.Windows.Forms.TextBox();
            this.accountKeyLabel = new System.Windows.Forms.Label();
            this.accountKeyTextBox = new System.Windows.Forms.TextBox();
            this.useHTTPSCheckBox = new System.Windows.Forms.CheckBox();
            this.uploadFolderTextBox = new System.Windows.Forms.TextBox();
            this.uploadFolderBrowseButton = new System.Windows.Forms.Button();
            this.uploadFolderLabel = new System.Windows.Forms.Label();
            this.blobContainerLabel = new System.Windows.Forms.Label();
            this.blobContainerTextBox = new System.Windows.Forms.TextBox();
            this.uploadButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.uploadFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.accountNameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.accountNameTextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.accountKeyLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.accountKeyTextBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.useHTTPSCheckBox, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.uploadFolderTextBox, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.uploadFolderBrowseButton, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.uploadFolderLabel, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.blobContainerLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.blobContainerTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.uploadButton, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.logTextBox, 0, 8);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // accountNameLabel
            // 
            resources.ApplyResources(this.accountNameLabel, "accountNameLabel");
            this.tableLayoutPanel1.SetColumnSpan(this.accountNameLabel, 3);
            this.accountNameLabel.Name = "accountNameLabel";
            // 
            // accountNameTextBox
            // 
            resources.ApplyResources(this.accountNameTextBox, "accountNameTextBox");
            this.tableLayoutPanel1.SetColumnSpan(this.accountNameTextBox, 3);
            this.errorProvider.SetIconPadding(this.accountNameTextBox, ((int)(resources.GetObject("accountNameTextBox.IconPadding"))));
            this.accountNameTextBox.Name = "accountNameTextBox";
            this.accountNameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.accountNameTextBox_Validating);
            // 
            // accountKeyLabel
            // 
            resources.ApplyResources(this.accountKeyLabel, "accountKeyLabel");
            this.tableLayoutPanel1.SetColumnSpan(this.accountKeyLabel, 3);
            this.accountKeyLabel.Name = "accountKeyLabel";
            // 
            // accountKeyTextBox
            // 
            resources.ApplyResources(this.accountKeyTextBox, "accountKeyTextBox");
            this.tableLayoutPanel1.SetColumnSpan(this.accountKeyTextBox, 3);
            this.errorProvider.SetIconPadding(this.accountKeyTextBox, ((int)(resources.GetObject("accountKeyTextBox.IconPadding"))));
            this.accountKeyTextBox.Name = "accountKeyTextBox";
            this.accountKeyTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.accountKeyTextBox_Validating);
            // 
            // useHTTPSCheckBox
            // 
            resources.ApplyResources(this.useHTTPSCheckBox, "useHTTPSCheckBox");
            this.useHTTPSCheckBox.Checked = true;
            this.useHTTPSCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useHTTPSCheckBox.Name = "useHTTPSCheckBox";
            this.useHTTPSCheckBox.UseVisualStyleBackColor = true;
            // 
            // uploadFolderTextBox
            // 
            resources.ApplyResources(this.uploadFolderTextBox, "uploadFolderTextBox");
            this.uploadFolderTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.uploadFolderTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.tableLayoutPanel1.SetColumnSpan(this.uploadFolderTextBox, 2);
            this.errorProvider.SetIconPadding(this.uploadFolderTextBox, ((int)(resources.GetObject("uploadFolderTextBox.IconPadding"))));
            this.uploadFolderTextBox.Name = "uploadFolderTextBox";
            this.uploadFolderTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.uploadFolderTextBox_Validating);
            // 
            // uploadFolderBrowseButton
            // 
            resources.ApplyResources(this.uploadFolderBrowseButton, "uploadFolderBrowseButton");
            this.uploadFolderBrowseButton.CausesValidation = false;
            this.uploadFolderBrowseButton.Name = "uploadFolderBrowseButton";
            this.uploadFolderBrowseButton.UseVisualStyleBackColor = true;
            this.uploadFolderBrowseButton.Click += new System.EventHandler(this.uploadFolderBrowseButton_Click);
            // 
            // uploadFolderLabel
            // 
            resources.ApplyResources(this.uploadFolderLabel, "uploadFolderLabel");
            this.tableLayoutPanel1.SetColumnSpan(this.uploadFolderLabel, 3);
            this.uploadFolderLabel.Name = "uploadFolderLabel";
            // 
            // blobContainerLabel
            // 
            resources.ApplyResources(this.blobContainerLabel, "blobContainerLabel");
            this.blobContainerLabel.Name = "blobContainerLabel";
            // 
            // blobContainerTextBox
            // 
            resources.ApplyResources(this.blobContainerTextBox, "blobContainerTextBox");
            this.tableLayoutPanel1.SetColumnSpan(this.blobContainerTextBox, 2);
            this.errorProvider.SetIconPadding(this.blobContainerTextBox, ((int)(resources.GetObject("blobContainerTextBox.IconPadding"))));
            this.blobContainerTextBox.Name = "blobContainerTextBox";
            this.blobContainerTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.blobContainerTextBox_Validating);
            // 
            // uploadButton
            // 
            resources.ApplyResources(this.uploadButton, "uploadButton");
            this.tableLayoutPanel1.SetColumnSpan(this.uploadButton, 3);
            this.uploadButton.Name = "uploadButton";
            this.uploadButton.UseVisualStyleBackColor = true;
            this.uploadButton.Click += new System.EventHandler(this.uploadButton_Click);
            // 
            // logTextBox
            // 
            resources.ApplyResources(this.logTextBox, "logTextBox");
            this.logTextBox.CausesValidation = false;
            this.tableLayoutPanel1.SetColumnSpan(this.logTextBox, 3);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            // 
            // uploadFolderBrowserDialog
            // 
            resources.ApplyResources(this.uploadFolderBrowserDialog, "uploadFolderBrowserDialog");
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // UploadForm
            // 
            this.AcceptButton = this.uploadButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UploadForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UploadForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FolderBrowserDialog uploadFolderBrowserDialog;
        private System.Windows.Forms.Label accountNameLabel;
        private System.Windows.Forms.TextBox accountNameTextBox;
        private System.Windows.Forms.Label uploadFolderLabel;
        private System.Windows.Forms.Label accountKeyLabel;
        private System.Windows.Forms.TextBox accountKeyTextBox;
        private System.Windows.Forms.TextBox uploadFolderTextBox;
        private System.Windows.Forms.Button uploadFolderBrowseButton;
        private System.Windows.Forms.CheckBox useHTTPSCheckBox;
        private System.Windows.Forms.Label blobContainerLabel;
        private System.Windows.Forms.TextBox blobContainerTextBox;
        private System.Windows.Forms.Button uploadButton;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}

