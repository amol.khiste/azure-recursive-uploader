﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Geometric.Toolbox.UI;

namespace Geometric.Azure.RecursiveUploader.UI
{
    public partial class UploadForm : Form
    {
        Process uploadProcess = null;
        private SystemMenu systemMenu; //http://stackoverflow.com/questions/4615940/how-can-i-customize-the-system-menu-of-a-windows-form

        public UploadForm()
        {
            InitializeComponent();

            systemMenu = new SystemMenu(this);
            systemMenu.AddCommand(Properties.Resources.SYSMENU_ABOUT, OnSysMenuAbout, true);
        }

        protected override void WndProc(ref Message msg)
        {
            base.WndProc(ref msg);

            // Let it know all messages so it can handle WM_SYSCOMMAND
            // (This method is inlined)
            systemMenu.HandleMessage(ref msg);
        }

        private void OnSysMenuAbout()
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog();
        }

        private void uploadFolderBrowseButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == uploadFolderBrowserDialog.ShowDialog())
            {
                uploadFolderTextBox.Text = uploadFolderBrowserDialog.SelectedPath;
            }
        }

        private void uploadButton_Click(object sender, EventArgs e)
        {
            if (!ValidateControls(this))
            {
                DialogResult = DialogResult.None;
                return;
            }
            if (!File.Exists(@"Uploader\azureuploader.exe"))
            {
                MessageBox.Show(@"Uploader\azureuploader.exe missing. Please copy and retry.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                DialogResult = DialogResult.None;
                return;
            }
            uploadButton.Enabled = false;

            Task uploadExeTask = new Task(new Action(StartUploadExe));
            uploadExeTask.ContinueWith(new Action<Task>(OnUploadTaskComplete));
            uploadExeTask.Start();
        }

        private void OnUploadTaskComplete(Task uploadExeTask)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<Task>(OnUploadTaskComplete), new[] {uploadExeTask});
            }
            else
            {
                Exception uploadTaskException = uploadExeTask.Exception;
                string errorMessages = null;
                while (null != uploadTaskException)
                {
                    errorMessages += uploadTaskException.ToString() + Environment.NewLine +
                        "================================================================================" +
                        Environment.NewLine;
                    uploadTaskException = uploadTaskException.InnerException;
                }
                if (!String.IsNullOrEmpty(errorMessages))
                {
                    logTextBox.AppendText(errorMessages);
                }
                logTextBox.AppendText("azureuploader.exe has exited.");
                uploadButton.Enabled = true;
            }
        }

        private void StartUploadExe()
        {
            uploadProcess = new Process();
            uploadProcess.StartInfo = new ProcessStartInfo()
            {
                FileName = @"Uploader\azureuploader.exe",
                Arguments = String.Format("-n {0} -k {1} -c {2} -f {3}",
                    accountNameTextBox.Text, accountKeyTextBox.Text, blobContainerTextBox.Text, uploadFolderTextBox.Text),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };
            uploadProcess.EnableRaisingEvents = true;
            uploadProcess.OutputDataReceived += new DataReceivedEventHandler(uploadProcess_OutputDataReceived);
            uploadProcess.ErrorDataReceived += new DataReceivedEventHandler(uploadProcess_ErrorDataReceived);
            uploadProcess.Start();
            uploadProcess.BeginOutputReadLine();
            uploadProcess.BeginErrorReadLine();
            uploadProcess.WaitForExit();
            if (0 != uploadProcess.ExitCode)
            {
                throw new ApplicationException("azureuploader.exe exited with code " + uploadProcess.ExitCode);
            }
        }

        void uploadProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<object, DataReceivedEventArgs>(uploadProcess_ErrorDataReceived), new[] { sender, e });
            }
            else
            {
                logTextBox.AppendText(e.Data + Environment.NewLine);
            }
        }

        void uploadProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<object, DataReceivedEventArgs>(uploadProcess_OutputDataReceived), new[] {sender, e});
            }
            else
            {
                logTextBox.AppendText(e.Data + Environment.NewLine);
            }
        }

        private bool ValidateControls(Control ctrl)
        {
            //Ref: https://msdn.microsoft.com/en-us/library/ms950965.aspx
            foreach (Control control in ctrl.Controls)
            {
                // Set focus on control
                control.Focus();
                // Validate causes the control's Validating event to be fired,
                // if CausesValidation is True
                if (!Validate())
                {
                    return false;
                }
                if (!ValidateControls(control))
                {
                    return false;
                }
            }
            return true;
        }

        private void accountNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            ValidateEmptyText((Control)sender, e);
        }

        private void accountKeyTextBox_Validating(object sender, CancelEventArgs e)
        {
            ValidateEmptyText((Control)sender, e);
        }

        private void blobContainerTextBox_Validating(object sender, CancelEventArgs e)
        {
            ValidateEmptyText((Control)sender, e);
        }

        private void uploadFolderTextBox_Validating(object sender, CancelEventArgs e)
        {
            ValidateEmptyText((Control)sender, e);
            if (!Directory.Exists(uploadFolderTextBox.Text))
            {
                errorProvider.SetError(uploadFolderTextBox, "Path not found");
                e.Cancel = true;
                return;
            }
            errorProvider.SetError(uploadFolderTextBox, "");
        }

        private void ValidateEmptyText(Control ctrl, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(ctrl.Text.Trim()))
            {
                errorProvider.SetError(ctrl, "Required");
                e.Cancel = true;
                return;
            }
            errorProvider.SetError(ctrl, "");
        }

        private void UploadForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;

            if (null != uploadProcess && !uploadProcess.HasExited)
            {
                if (DialogResult.OK == MessageBox.Show(Properties.Resources.UPLOAD_CANCEL_PROMPT, Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation))
                {
                    if (null != uploadProcess && !uploadProcess.HasExited)
                    {
                        uploadProcess.Kill();
                        uploadProcess = null;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

    }
}
